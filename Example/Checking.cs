﻿using System;
using CoreFx.Notifications;
using CoreFx.Validations;
using DataTransferObjects.Network;
using DataTransferObjects.Network.Validations;
using DummySampleInUse.Entities;

namespace DummySampleInUse
{
    class Checking
    {
        protected Checking() { }
        
        static void Main()
        {
           
            NetworkEntityCrud newtworkEntity = new NetworkEntityCrud();

            INetworkValidation networkValidations = new NetworkBasicValidations();

            ((IInjectValidation<INetworkValidation>)newtworkEntity).SourceValidation = networkValidations;
            
            newtworkEntity.Notifications += new Handlers.NotificationsHandler<NotificationsEventArgs>(HandlerErrors)
                                         +  new Handlers.NotificationsHandler<NotificationsEventArgs>(HandlerErrorsAdvance);

            NetworkAddress networkAddress = WrapperNetworkAddress.WrongAddress();
            newtworkEntity.Insert(networkAddress);

            Console.ReadKey();

        }


        private static void HandlerErrors(object sender, NotificationsEventArgs e)
        {
            e.Messeges.ForEach ( (string message) => { Console.WriteLine("Error -> " + message); });
        }

        private static void HandlerErrorsAdvance(object sender, NotificationsEventArgs e)
        {
            e.Messeges.ForEach((string message) => { Console.WriteLine($"Error { message } sent to repository "); });
        }

    }

    internal static class WrapperNetworkAddress
    {
        internal static NetworkAddress GoodAddress()
        {
            NetworkAddress address = new NetworkAddress();

            address.Ip = "127.0.0.1";
            address.Mac = "00-14-22-01-23-45";
            address.Serie = "Vx09";

            return address;
        }


        internal static NetworkAddress WrongAddress()
        {
            NetworkAddress address = new NetworkAddress();

            address.Ip = "127.2.0.B";
            address.Mac = "00:0a:95:9d:68:16";
            address.Serie = null;

            return address;
        }
    }
}
