﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CoreFx.Assertions
{
    public class EntityAssertions<T> : IEnumerable<Predicate<T>> where T : class
    {
        private readonly List<Predicate<T>> assertions = new List<Predicate<T>>();
        
        public T Source { private get; set; }

        public Predicate<T> this[int index]
        {
            get { return assertions.ElementAt(index); }
            set { assertions.Insert(index, value); }
        }

        /// <summary> Add a new predicate in the collection </summary>
        public void Add(Predicate<T> assertion) { assertions.Add(assertion); }

        /// <summary> Remove a predicate from the collection </summary>
        public void Remove(Predicate<T> assertion) { assertions.Remove(assertion); }

        /// <summary> Evaluates until a predicate is found as false then it doesn't search any more. </summary>
        public bool EvaluateAll() => assertions.All(val => val(Source));

        /// <summary> Evaluates until a predicate is found as true then it doesn't search any more. </summary>
        public bool EvaluateAny() => assertions.Any(val => val(Source));

        /// <summary> Evaluates all predicates. </summary>
        public bool EvaluateIntersect()
        {
            bool isTrue = true;

            assertions
            .ForEach
                (
                    (Predicate<T> val) => 
                        {
                            bool isTrueThePredicate = val(Source);
                            if (isTrue)
                                isTrue =  isTrueThePredicate;
                        }
                );

            return isTrue;
        }

        public IEnumerator<Predicate<T>> GetEnumerator() => assertions.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

    }
}
